const findingAverageOfSalariesBasedOnCountry = (correctedSalaries) => {



    let averageSalariesBasedOnCountry = correctedSalaries.reduce((allCountries, employee) => {

        let country = employee.location

        if (country in allCountries) {
            allCountries[country]["sum"] += parseFloat((employee["corrected_salary"]))
            allCountries[country]["count"] += 1
            allCountries[country]["average"] = parseFloat((allCountries[country]["sum"] / allCountries[country]["count"]).toFixed(2))
        }
        else {
            allCountries[country] = { "sum": employee["corrected_salary"], "count": 1, "average": employee["corrected_salary"] }
        }
        return allCountries
    }, {})


    let avgSalariesPerCountry = Object.entries(averageSalariesBasedOnCountry).reduce((avgSalaries, country) => {
        let location = country[0]
        let obj = country[1]
        avgSalaries[location] = obj["average"]
        return avgSalaries

    }, {})

    return avgSalariesPerCountry

}
module.exports = findingAverageOfSalariesBasedOnCountry