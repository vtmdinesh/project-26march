const findingSumOfSalaries = (correctedSalaries) => {
    let sumOfSalaries = correctedSalaries.reduce((acc, value) => {

        acc = acc + value.corrected_salary

        return parseFloat(acc.toFixed(3))
    }, 0)
    return sumOfSalaries

}
module.exports = findingSumOfSalaries