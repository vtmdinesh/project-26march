const convertingSalariesWithfactor = (employeeDetails) => {
    let convertedSalariesArray = employeeDetails.map(employee => {
        employee.corrected_salary = parseFloat((employee.salary.replace("$", ""))) * 10000
        return employee

    })
    return convertedSalariesArray

}
module.exports = convertingSalariesWithfactor