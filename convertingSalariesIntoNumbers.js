const convertingSalariesIntoNumbers = (employeeDetails) => {
    let salaryDetails = employeeDetails.map(employee => {
        employee.salary = parseFloat((employee.salary.replace("$", "")))
        return employee

    })
    return salaryDetails

}
module.exports = convertingSalariesIntoNumbers