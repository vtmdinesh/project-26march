const findingAverageOfSaleriesBasedOnCountry = (employeeDetails) => {



    let averageSaleriesBasedOnCountry = employeeDetails.reduce((allCountries, employee) => {

        let country = employee.location

        if (country in allCountries) {
            allCountries[country]["sum"] += parseFloat((employee.salary.replace("$", "")))
            allCountries[country]["count"] += 1
            allCountries[country]["average"] = parseFloat((allCountries[country]["sum"] / allCountries[country]["count"]).toFixed(2))
        }
        else {
            allCountries[country] = { "sum": 0, "count": 0, "average": 0 }
            allCountries[country]["sum"] = parseFloat((employee.salary.replace("$", "")))
            allCountries[country]["count"] = 1
            allCountries[country]["average"] = parseFloat((allCountries[country]["sum"] / allCountries[country]["count"]).toFixed(2))
        }
        return allCountries
    }, {})
    let avgSalaries = {}
    Object.entries(averageSaleriesBasedOnCountry).forEach(country => {
        let location = country[0]
        let obj = country[1]
        avgSalaries[location] = obj["average"]

    })

    return avgSalaries

}
module.exports = findingAverageOfSaleriesBasedOnCountry