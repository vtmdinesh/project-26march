const findingSumOfSaleriesBasedOnCountry = (employeeDetails) => {



    let findingSumOfSaleriesBasedOnCountry = employeeDetails.reduce((allCountries, employee) => {

        let country = employee.location

        if (country in allCountries) {
            allCountries[country] += parseFloat((employee.salary.replace("$", "")))
        }
        else {
            allCountries[country] = parseFloat((employee.salary.replace("$", "")))
        }
        return allCountries
    }, {})

    return findingSumOfSaleriesBasedOnCountry

}
module.exports = findingSumOfSaleriesBasedOnCountry