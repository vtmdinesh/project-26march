const findingSumOfSalariesBasedOnCountry = (correctedSalaryDetails) => {

    let findingSumOfSalariesBasedOnCountry = correctedSalaryDetails.reduce((allCountries, employee) => {

        let country = employee.location

        if (country in allCountries) {
            allCountries[country] += employee.corrected_salary
        }
        else {
            allCountries[country] = employee.corrected_salary
        }
        return allCountries
    }, {})

    return findingSumOfSalariesBasedOnCountry

}
module.exports = findingSumOfSalariesBasedOnCountry